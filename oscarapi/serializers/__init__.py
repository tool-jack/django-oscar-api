from oscarapi.serializers.basket import *
from oscarapi.serializers.login import *
from oscarapi.serializers.product import *
from oscarapi.serializers.checkout import *
from oscarapi.serializers.catalogue import *
from oscarapi.serializers.search import *
