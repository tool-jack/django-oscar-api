from django.shortcuts import get_object_or_404
import logging
from rest_framework import serializers

from django.utils.translation import ugettext as _
from oscar.core.loading import get_model

logger = logging.getLogger(__name__)

Category = get_model('catalogue', 'Category')


class CategorySerializer(serializers.ModelSerializer):
    path_repr = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = '__all__'

    def get_path_repr(self, obj):
        return obj.full_name
