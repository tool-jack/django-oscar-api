import warnings

from django.db import IntegrityError
from rest_framework.response import Response

from django.conf import settings
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.translation import gettext as _
from oscar.core import prices
from oscar.core.loading import get_class, get_model
from rest_framework import exceptions, serializers

from oscarapi.basket.operations import (
    assign_basket_strategy,
)
from oscarapi.serializers import (
    VoucherSerializer,
    OfferDiscountSerializer,
    ProductsSerializer,
)
from oscarapi.utils import (
    OscarHyperlinkedModelSerializer,
    OscarModelSerializer,
    overridable
)
from oscarapi.serializers.fields import TaxIncludedDecimalField


OrderPlacementMixin = get_class('checkout.mixins', 'OrderPlacementMixin')
OrderTotalCalculator = get_class('checkout.calculators',
                                 'OrderTotalCalculator')
ShippingAddress = get_model('order', 'ShippingAddress')
BillingAddress = get_model('order', 'BillingAddress')
Order = get_model('order', 'Order')
OrderLine = get_model('order', 'Line')
OrderLineAttribute = get_model('order', 'LineAttribute')

Basket = get_model('basket', 'Basket')
Country = get_model('address', 'Country')
Repository = get_class('shipping.repository', 'Repository')

UserAddress = get_model('address', 'UserAddress')


class PriceSerializer(serializers.Serializer):
    currency = serializers.CharField(
        max_length=12, default=settings.OSCAR_DEFAULT_CURRENCY, required=False)
    excl_tax = serializers.DecimalField(
        decimal_places=2, max_digits=12, required=True)
    incl_tax = TaxIncludedDecimalField(
        excl_tax_field='excl_tax',
        decimal_places=2, max_digits=12, required=False)
    incl_tax_excl_discounts = serializers.SerializerMethodField()
    incl_tax_incl_discounts = serializers.SerializerMethodField()

    class Meta:
        fields = ('currency', 'excl_tax', 'incl_tax', 'incl_tax_incl_discounts')

    def __init__(self, instance=None, *args, **kwargs):
        self.incl_tax_excl_discounts = kwargs.pop('incl_tax_excl_discounts', None)
        super(PriceSerializer, self).__init__(instance=instance, *args, **kwargs)

    def get_incl_tax_excl_discounts(self, obj):
        if self.incl_tax_excl_discounts is not None:
            return "{}".format(self.incl_tax_excl_discounts)
        elif obj.is_tax_known:
            return "{}".format(obj.incl_tax)
        else:
            return "{}".format(obj.excl_tax)

    def get_incl_tax_incl_discounts(self, obj):
        if obj.is_tax_known:
            return "{}".format(obj.incl_tax)
        else:
            return "{}".format(obj.excl_tax)


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class ShippingAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShippingAddress
        fields = '__all__'


class InlineShippingAddressSerializer(OscarModelSerializer):

    class Meta:
        model = ShippingAddress
        fields = '__all__'


class BillingAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingAddress
        fields = '__all__'


class InlineBillingAddressSerializer(OscarModelSerializer):

    class Meta:
        model = BillingAddress
        fields = '__all__'


class ShippingMethodSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=128)
    name = serializers.CharField(max_length=128)
    price = serializers.SerializerMethodField('calculate_price')

    def calculate_price(self, obj):
        price = obj.calculate(self.context.get('basket'))
        try:
            base_charge = obj.method.calculate(self.context.get('basket')).incl_tax
        except AttributeError:
            base_charge = obj.calculate(self.context.get('basket')).incl_tax
        return PriceSerializer(price, incl_tax_excl_discounts=base_charge, context=self.context).data


class PaymentMethodSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=50)
    name = serializers.CharField(max_length=128)


class OrderLineAttributeSerializer(OscarHyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='order-lineattributes-detail')

    class Meta:
        model = OrderLineAttribute
        fields = '__all__'


class OrderLineSerializer(serializers.ModelSerializer):
    "This serializer renames some fields so they match up with the basket"

    attributes = OrderLineAttributeSerializer(
        many=True, fields=('url', 'option', 'value'), required=False)
    product = ProductsSerializer(read_only=True)
    price = serializers.SerializerMethodField()

    class Meta:
        model = OrderLine
        fields = overridable('OSCAR_ORDERLINE_FIELD', default=[
            'attributes', 'id', 'product', 'stockrecord', 'quantity', 'price',
            'order'])

    def get_price(self, obj):
        return {
            "currency": obj.order.currency,
            "excl_tax_incl_discounts": "%.2f" % obj.line_price_excl_tax,
            "incl_tax_incl_discounts": "%.2f" % obj.line_price_incl_tax,
            "excl_tax_excl_discounts": "%.2f" % obj.line_price_before_discounts_excl_tax,
            "incl_tax_excl_discounts": "%.2f" % obj.line_price_before_discounts_incl_tax,
            "tax": "%.2f" % obj.line_price_tax,
        }


class OrderOfferDiscountSerializer(OfferDiscountSerializer):
    name = serializers.CharField(source='offer_name')
    amount = serializers.DecimalField(decimal_places=2, max_digits=12)


class OrderVoucherOfferSerializer(OrderOfferDiscountSerializer):
    voucher = VoucherSerializer(required=False)


class OrderSerializer(serializers.ModelSerializer):
    """
    Order Detail Serializer
    The order serializer tries to have the same kind of structure as the
    basket. That way the same kind of logic can be used to display the order
    as the basket in the checkout process.
    """
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True, source='user')
    lines = OrderLineSerializer(many=True)
    shipping_address = InlineShippingAddressSerializer(
        many=False, required=False)
    billing_address = InlineBillingAddressSerializer(
        many=False, required=False)

    total = serializers.SerializerMethodField()
    shipping = serializers.SerializerMethodField()

    payment_url = serializers.SerializerMethodField()
    offer_discounts = serializers.SerializerMethodField()
    voucher_discounts = serializers.SerializerMethodField()
    shipping_discount = serializers.SerializerMethodField()

    def get_offer_discounts(self, obj):
        qs = obj.basket_discounts.filter(offer_id__isnull=False)
        return OrderOfferDiscountSerializer(qs, many=True).data

    def get_voucher_discounts(self, obj):
        qs = obj.basket_discounts.filter(voucher_id__isnull=False)
        return OrderVoucherOfferSerializer(qs, many=True).data

    def get_payment_url(self, obj):
        try:
            return reverse('api-payment', args=(obj.pk,))
        except NoReverseMatch:
            msg = "You need to implement a view named 'api-payment' " \
                "which redirects to the payment provider and sets up the " \
                "callbacks."
            warnings.warn(msg)
            return msg

    def get_total(self, obj):
        return get_total_from_object(obj)

    def get_shipping(self, obj):
        return get_shipping_from_object(obj)

    def get_shipping_discount(self, obj):
        shipping_discount = obj.shipping_discounts.first()
        if shipping_discount is not None:
            return {
                'name': shipping_discount.offer_name,
                'voucher': shipping_discount.voucher_id,
                'freq': shipping_discount.frequency,
                'discount': shipping_discount.amount,
            }
        return None

    class Meta:
        model = Order
        fields = overridable('OSCARAPI_ORDER_FIELD', default=(
            'number', 'basket', 'id', 'lines',
            'owner', 'billing_address', 'total', 'shipping',
            'shipping_address', 'status', 'guest_email',
            'date_placed', 'payment_url', 'offer_discounts',
            'voucher_discounts', 'shipping_discount',)
        )


class OrderListSerializer(serializers.ModelSerializer):
    """
    Order List Serializer
    The order serializer tries to have the same kind of structure as the
    basket. That way the same kind of logic can be used to display the order
    as the basket in the checkout process.
    """
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True, source='user')
    lines = serializers.SerializerMethodField()

    total = serializers.SerializerMethodField()
    shipping = serializers.SerializerMethodField()

    def get_total(self, obj):
        return get_total_from_object(obj)

    def get_shipping(self, obj):
        return get_shipping_from_object(obj)

    def get_lines(self, obj):
        return obj.lines.count()

    class Meta:
        model = Order
        fields = overridable('OSCARAPI_ORDER_FIELD', default=(
            'number', 'basket', 'id', 'lines', 'owner',
            'total', 'shipping', 'status', 'guest_email',
            'date_placed',)
        )


def get_total_from_object(obj):
    """
    Orders and Order Serializer serializers.SerializeMethod
    to get total from the object
    """
    return {
        'currency': obj.currency,
        'incl_tax_incl_discounts': "%.2f" % obj.total_incl_tax,
        'excl_tax_incl_discounts': "%.2f" % obj.total_excl_tax,
        'incl_tax_excl_discounts': "%.2f" % obj.total_before_discounts_incl_tax,
        'excl_tax_excl_discounts': "%.2f" % obj.total_before_discounts_excl_tax,
        'tax': "%.2f" % obj.total_tax,
    }


def get_shipping_from_object(obj):
    """
    Orders and Order Serializer serializers.SerializeMethod
    to get shipping details from the object
    """
    return {
        'code': obj.shipping_code,
        'name': obj.shipping_method,
        'price': {
            'currency': obj.currency,
            'incl_tax': "%.2f" % obj.shipping_incl_tax,
            'excl_tax': "%.2f" % obj.shipping_excl_tax,
            'incl_tax_excl_discounts': "%.2f" % obj.shipping_before_discounts_incl_tax,
            'incl_tax_incl_discounts': "%.2f" % obj.shipping_incl_tax,
        }
    }


class ShippingSerializer(serializers.Serializer):
    code = serializers.CharField(
        max_length=128, required=False)
    name = serializers.CharField(
        max_length=256, required=False)
    price = PriceSerializer(required=False)


class CheckoutSerializer(serializers.Serializer, OrderPlacementMixin):
    basket = serializers.PrimaryKeyRelatedField(queryset=Basket.objects)
    guest_email = serializers.EmailField(allow_blank=True, required=False)
    total = serializers.DecimalField(
        decimal_places=2, max_digits=12, required=False)
    shipping = ShippingSerializer()
    shipping_address = ShippingAddressSerializer(many=False, required=False)
    billing_address = BillingAddressSerializer(many=False, required=False)

    def get_initial_order_status(self, basket):
        return overridable('OSCARAPI_INITIAL_ORDER_STATUS', default='new')

    def validate(self, attrs):
        request = self.context['request']

        if request.user.is_anonymous():
            if not settings.OSCAR_ALLOW_ANON_CHECKOUT:
                message = _('Anonymous checkout forbidden')
                raise serializers.ValidationError(message)

            if not attrs.get('guest_email'):
                # Always require the guest email field if the user is anonymous
                message = _('Guest email is required for anonymous checkouts')
                raise serializers.ValidationError(message)
        else:
            if 'guest_email' in attrs:
                # Don't store guest_email field if the user is authenticated
                del attrs['guest_email']

        basket = attrs.get('basket')
        basket = assign_basket_strategy(basket, request)
        if basket.num_items <= 0:
            message = _('Cannot checkout with empty basket')
            raise serializers.ValidationError(message)

        if 'shipping' not in attrs:
            message = _('Shipping code and price is required')
            raise serializers.ValidationError(message)

        if 'shipping_address' not in attrs:
            message = _('Shipping address is required')
            raise serializers.ValidationError(message)

        shipping_method = self._shipping_method(
            request, basket,
            attrs.get('shipping').get('code'),
            attrs.get('shipping_address')
        )
        shipping_charge = shipping_method.calculate(basket)
        posted_shipping_charge = attrs.get('shipping').get('price')

        if posted_shipping_charge is not None:
            posted_shipping_charge = prices.Price(**posted_shipping_charge)
            # test submitted data.
            if not posted_shipping_charge == shipping_charge:
                message = _('Shipping price incorrect %s != %s' % (
                    posted_shipping_charge, shipping_charge
                ))
                raise serializers.ValidationError(message)

        posted_total = attrs.get('total')
        total = OrderTotalCalculator().calculate(basket, shipping_charge)
        if posted_total is not None:
            if posted_total != total.incl_tax:
                message = _('Total incorrect %s != %s' % (
                    posted_total,
                    total.incl_tax
                ))
                raise serializers.ValidationError(message)

        # update attrs with validated data.
        attrs['total'] = total
        attrs['shipping_method'] = shipping_method
        attrs['shipping_charge'] = shipping_charge
        attrs['basket'] = basket
        return attrs

    def create(self, validated_data):
        try:
            basket = validated_data.get('basket')
            order_number = self.generate_order_number(basket)
            request = self.context['request']

            if 'shipping_address' in validated_data:
                shipping_address = ShippingAddress(
                    **validated_data['shipping_address'])
            else:
                shipping_address = None

            if 'billing_address' in validated_data:
                billing_address = BillingAddress(
                    **validated_data['billing_address'])
            else:
                billing_address = None

            return self.place_order(
                order_number=order_number,
                user=request.user,
                basket=basket,
                shipping_address=shipping_address,
                shipping_method=validated_data.get('shipping_method'),
                shipping_charge=validated_data.get('shipping_charge'),
                billing_address=billing_address,
                order_total=validated_data.get('total'),
                guest_email=validated_data.get('guest_email') or ''
            )
        except ValueError as e:
            raise exceptions.NotAcceptable(str(e))

    def _shipping_method(self, request, basket,
                         shipping_method_code, shipping_address):
        repo = Repository()

        default = repo.get_default_shipping_method(
            basket=basket,
            user=request.user,
            request=request,
            shipping_addr=shipping_address
        )

        if shipping_method_code is not None:
            methods = repo.get_shipping_methods(
                basket=basket,
                user=request.user,
                request=request,
                shipping_addr=shipping_address
            )

            find_method = (
                s for s in methods if s.code == shipping_method_code)
            shipping_method = next(find_method, default)
            return shipping_method

        return default


class UserAddressSerializer(serializers.ModelSerializer):
    country_name = serializers.SerializerMethodField()

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        try:
            return super(UserAddressSerializer, self).create(validated_data)
        except IntegrityError as e:
            raise exceptions.NotAcceptable(str(e))

    def update(self, instance, validated_data):
        # to be sure that we cannot change the owner of an address. If you
        # want this, please override the serializer
        request = self.context['request']
        validated_data['user'] = request.user
        try:
            return super(
                UserAddressSerializer, self).update(instance, validated_data)
        except IntegrityError as e:
            raise exceptions.NotAcceptable(str(e))

    class Meta:
        model = UserAddress
        fields = overridable('OSCARAPI_USERADDRESS_FIELDS', (
            'id', 'title', 'first_name', 'last_name', 'line1', 'line2',
            'line3', 'line4', 'state', 'postcode', 'search_text',
            'phone_number', 'notes', 'is_default_for_shipping',
            'is_default_for_billing', 'country', 'country_name',))

    def get_country_name(self, obj):
        return obj.country.name
