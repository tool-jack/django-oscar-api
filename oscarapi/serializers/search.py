from drf_haystack.serializers import HaystackSerializer
from oscar.apps.search.search_indexes import ProductIndex
from rest_framework import serializers
from oscar.core.loading import get_model
from django.conf import settings

from ..serializers.product import get_product_price, get_product_availability

Product = get_model('catalogue', 'Product')


class ProductSearchSerializer(HaystackSerializer):
    id = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    availability = serializers.SerializerMethodField()
    image_primary = serializers.SerializerMethodField()

    class Meta:
        # The `index_classes` attribute is a list of which search indexes
        # we want to include in the search.
        index_classes = [ProductIndex]

        # The `fields` contains all the fields we want to include.
        # NOTE: Make sure you don't confuse these with model attributes. These
        # fields belong to the search index!
        fields = [
            'id', 'upc', 'title', 'product_class',
            'category', 'price',
        ]

    def get_id(self, obj):
        # return str(obj.id).split(".")[2]
        product_id = str(obj.id).split(".")[2]
        product = Product.objects.get(id=product_id)
        self.price = get_product_price(product, context=self.context)
        self.availability = get_product_availability(product)
        try:
            self.image_primary = '{}{}'.format(settings.MEDIA_URL,
                                               product.primary_image().original)
        except AttributeError:
            self.image_primary = None

        return product_id

    def get_price(self, obj):
        return self.price

    def get_availability(self, obj):
        return self.availability

    def get_image_primary(self, obj):
        return self.image_primary
