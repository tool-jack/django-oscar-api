from oscarapi.views.root import *
from oscarapi.views.basic import *
from oscarapi.views.login import *
from oscarapi.views.basket import *
from oscarapi.views.checkout import *
from oscarapi.views.product import *
from oscarapi.views.catalogue import *
from oscarapi.views.search import *
