from django.shortcuts import get_object_or_404

from oscar.core.loading import get_model
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.decorators import authentication_classes, permission_classes

from oscarapi import serializers, permissions

Category = get_model('catalogue', 'Category')
Product = get_model('catalogue', 'Product')


# to allow access to any use either
# @authentication_classes([])
# @permission_classes([])
# or
@permission_classes((AllowAny, ))
class CategoryList(generics.ListAPIView):
    serializer_class = serializers.CategorySerializer
    queryset = Category.objects.all()


@permission_classes((AllowAny, ))
class CategoryDetail(generics.RetrieveAPIView):
    serializer_class = serializers.CategorySerializer
    queryset = Category.objects.all()


@permission_classes((AllowAny, ))
class CategoryProducts(generics.ListAPIView):
    serializer_class = serializers.ProductsSerializer

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        if pk is not None:
            category = get_object_or_404(Category, id=pk)
            return Product.objects.filter(
                categories__path__startswith=category.path).all()
        else:
            return Product.objects.none()
