from oscar.core.loading import get_model

from oscarapi import permissions

from rest_framework import exceptions, generics

__all__ = ('BasketPermissionMixin',)

Basket = get_model('basket', 'Basket')


def parse_basket_from_primarykey(DATA, format):  # noqa
    return generics.get_object_or_404(Basket, id=DATA.get('basket'))


class BasketPermissionMixin(object):
    """
    This mixins adds some methods that can be used to check permissions
    on a basket instance.
    """
    # The permission class is mainly used to check Basket permission!
    permission_classes = (permissions.IsAdminUserOrRequestContainsBasket,)

    def get_data_basket(self, DATA, format):  # noqa
        return parse_basket_from_primarykey(DATA, format)

    def check_basket_permission(self, request, basket_pk=None, basket=None):
        "Check if the user may access this basket"
        if basket is None:
            basket = generics.get_object_or_404(Basket.objects, pk=basket_pk)
        self.check_object_permissions(request, basket)
        return basket
